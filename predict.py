#!/usr/bin/env python

import sys
from load_datasets import *
from train import MODEL_FILE

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")

def predict(recording, loaded_model):
    if not torch.cuda.is_available():
        loaded_model.device = torch.device("cpu")

    # Takes a single recording as input and outputs the predicted classes and probabilities
    classes = loaded_model.classes

    recording = pad_or_truncate(recording, SEQ_LENGTH)
    recording = torch.tensor([recording], dtype=torch.float32).to(device) # To 3D tensor, shape (1, SEQ_LENGTH, num_heads=12)

    current_score = torch.sigmoid(loaded_model(recording))

    current_label = current_score.clone()
    threshold = 0.75
    current_label[current_label >= threshold] = 1
    current_label[current_label < threshold] = 0

    current_label = current_label.cpu().to(torch.int8).data.numpy()[0]
    current_score = current_score.cpu().data.numpy()[0]
    current_score = np.array(list(map(lambda x: f"{x:.2f}", current_score))) # get 2 digits after the decimal point for each score.

    return current_label, current_score, classes

def load_checkpoint(modelpath="model/"+MODEL_FILE):
    if torch.cuda.is_available():
        checkpoint = torch.load(modelpath)
    else:
        checkpoint = torch.load(modelpath, map_location=torch.device("cpu"))

    model = checkpoint['model']
    model.load_state_dict(checkpoint['state_dict'])
    for parameter in model.parameters():
        parameter.requires_grad = False

    model.eval()
    return model

def save_challenge_predictions(output_directory, filename, scores, labels, classes):
    recording = os.path.splitext(filename)[0]
    new_file = filename.replace('.hea','.csv')
    output_file = os.path.join(output_directory,new_file)

    # Include the filename as the recording number
    recording_string = '#{}'.format(recording)
    class_string = ','.join(classes)
    label_string = ','.join(str(i) for i in labels)
    score_string = ','.join(str(i) for i in scores)

    with open(output_file, 'w') as f:
        f.write(recording_string + '\n' + class_string + '\n' + label_string + '\n' + score_string + '\n')

if __name__ == '__main__':
    # Parse arguments.
    if len(sys.argv) != 4:
        raise Exception('Include the input and output directories as arguments, e.g., python predict.py model input output.')

    model_directory = sys.argv[1]
    input_directory = sys.argv[2]
    output_directory = sys.argv[3]

    datatest = PhysioNetDataset(data_dir=input_directory, train=False)
    test_files = datatest.header_files.tolist()

    if not os.path.isdir(output_directory):
        os.mkdir(output_directory)

    print('Loading model...')
    model = load_checkpoint(model_directory)

    print('Predicting each recording...')
    num_files = len(test_files)

    for i, f in enumerate(test_files):
        if i % 100 == 0:
            print('    {}/{}...'.format(i, num_files))
        recording = load_challenge_data_recording(f)
        current_label, current_score,classes = predict(recording, model)
        # Save results.
        save_challenge_predictions(output_directory, f.split('/')[-1], current_score, current_label, classes)

    print('Done.')
