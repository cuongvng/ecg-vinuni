import torch
import torch.nn as nn
import math
from load_datasets import SEQ_LENGTH
import torch.nn.functional as F

class Conv_Transformer(nn.Module):
	def __init__(self, classes, n_features=12, n_layers=4, dim_feedforward=256, n_heads=4, dropout=0.1):
		self.classes = classes
		super(Conv_Transformer, self).__init__()

		# Convolutional Block
		self.conv = nn.Sequential(
			nn.Conv1d(in_channels=n_features, out_channels=64, kernel_size=7, stride=2), # 5000 -> 2497
			nn.BatchNorm1d(num_features=64),
			nn.ReLU(),
			nn.MaxPool1d(kernel_size=3, stride=2, padding=1), # 2497 -> 1249

			ResidualBlock(in_channels=64, out_channels=64),
			ResidualBlock(in_channels=64, out_channels=64),

			ResidualBlock(in_channels=64, out_channels=128, stride=2, use_1x1_conv=True), # 1249 -> 625
			ResidualBlock(in_channels=128, out_channels=128),

			ResidualBlock(in_channels=128, out_channels=256, stride=2, use_1x1_conv=True), # 625 -> 313
			ResidualBlock(in_channels=256, out_channels=256),

			ResidualBlock(in_channels=256, out_channels=512, stride=2, use_1x1_conv=True), # 313 -> 157
			ResidualBlock(in_channels=512, out_channels=512),
		)

		length = 157
		embed_dim = 512

		# Positional Encoding
		self.positional_encoder = PositionalEncoding(d_model=embed_dim, dropout=dropout, max_len=length)

		# Transformer Encoder
		encoder_layer = nn.TransformerEncoderLayer(d_model=embed_dim, nhead=n_heads, dropout=dropout,
											  activation="relu", dim_feedforward=dim_feedforward)

		self.transformer_encoder = nn.TransformerEncoder(encoder_layer=encoder_layer, num_layers=n_layers)

		# Fully-connected layer to generate output
		self.fc = nn.Linear(in_features=length*embed_dim, out_features=len(self.classes))

	def forward(self, X):
		X = X.permute(0, 2, 1)  # convert input format to (batch_size, n_features, seq_len)
		X = self.conv(X)

		X = X.permute(2, 0, 1) # (seq_len, batch_size, n_features)
		X = self.positional_encoder(X)

		X = X.transpose(0, 1) # (batch_size, seq_len, n_features)
		X  = self.transformer_encoder(X)

		X = X.flatten(start_dim=1)
		return self.fc(X)


class PositionalEncoding(nn.Module):
	def __init__(self, d_model, dropout=0.1, max_len=SEQ_LENGTH):
		super(PositionalEncoding, self).__init__()
		self.dropout = nn.Dropout(p=dropout)

		pe = torch.zeros(max_len, d_model)
		position = torch.arange(0, max_len, dtype=torch.float).unsqueeze(1)
		div_term = torch.exp(torch.arange(0, d_model, 2).float() * (-math.log(10000.0) / d_model))
		pe[:, 0::2] = torch.sin(position * div_term)
		pe[:, 1::2] = torch.cos(position * div_term)
		pe = pe.unsqueeze(0).transpose(0, 1)
		self.register_buffer('pe', pe)

	def forward(self, x):
		x = x + self.pe[:x.size(0), :]
		return self.dropout(x)

class ResidualBlock(nn.Module):
	def __init__(self, in_channels, out_channels, stride=1, use_1x1_conv=False):
		super(ResidualBlock, self).__init__()

		self.use_1x1_conv = use_1x1_conv
		self.conv1 = nn.Conv1d(in_channels, out_channels, kernel_size=3, padding=1, stride=stride)
		self.bn1 = nn.BatchNorm1d(num_features=out_channels)
		self.conv2 = nn.Conv1d(out_channels, out_channels, kernel_size=3, padding=1)
		self.bn2 = nn.BatchNorm1d(num_features=out_channels)
		self.conv1x1 = nn.Conv1d(in_channels, out_channels, kernel_size=1, stride=stride)

	def forward(self, X):
		X_original = X.clone()

		X = self.conv1(X)
		X = F.relu(self.bn1(X))
		X = self.conv2(X)
		X = self.bn2(X)

		if self.use_1x1_conv:
			X_original = self.conv1x1(X_original)

		return F.relu(X + X_original)


def xavier_init_weights(model):
	if isinstance(model, torch.nn.Linear) or isinstance(model, torch.nn.Conv1d):
		torch.nn.init.xavier_uniform_(model.weight)
	if isinstance(model, torch.nn.LSTM) or isinstance(model, torch.nn.GRU):
		torch.nn.init.xavier_uniform_(model.weight_ih_l0)
		torch.nn.init.xavier_uniform_(model.weight_hh_l0)
		try:
			torch.nn.init.xavier_uniform_(model.weight_ih_l1)
			torch.nn.init.xavier_uniform_(model.weight_hh_l1)
		except AttributeError:
			pass
