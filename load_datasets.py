import torch
from torch.utils.data import Dataset
from torch.utils.data.sampler import Sampler
import numpy as np
import os
import pandas as pd
from scipy.io import loadmat
import math
import random


# Out of 42511 samples, 35948 samples (84%) have sequence length = 5000.
# So we truncate or pad other sequence to have that length
SEQ_LENGTH = 5000


ABNORMALITIES = [
    '164951009', '164889003', '713427006',
    '39732003', '164873001', '164865005',
    '164861001', '427084000', '426783006',
    '270492004', '426177001', '445118002',
    '164909002',
]

EQUIVALENT_ABNORMALITIES = {
    '59118001': '713427006',
}


class PhysioNetDataset(Dataset):
    def __init__(self, data_dir, purpose="train", target_len=SEQ_LENGTH):
        assert purpose == "train" or purpose == "test"
        # Get all .hea files
        header_files = []
        for f in os.listdir(data_dir):
            g = os.path.join(data_dir, f)
            if not f.lower().startswith('.') and f.lower().endswith('hea') and os.path.isfile(g):
                header_files.append(g)

        header_files = pd.Series(header_files).sample(frac=1, random_state=1)
        # Split into 90% train, 10% test
        train_headers, test_headers = np.split(header_files.values, [int(0.9 * len(header_files))])
        if purpose == "train":
            self.header_files = pd.Series(train_headers)
        else:
            self.header_files = pd.Series(test_headers)

        self.target_len = target_len
        self.classes = ABNORMALITIES
        self.labels = self.get_labels()

    def __len__(self):
        return len(self.header_files)

    def __getitem__(self, idx: int):
        header_file = self.header_files.iloc[idx]
        # Get X
        recording = load_challenge_data_recording(header_file)
        # Pad or truncate
        recording = pad_or_truncate(recording, self.target_len)
        # Convert to torch tensor
        recording = torch.tensor(recording, dtype=torch.float32)

        # Get y
        header = load_challenge_data_header(header_file)
        label = get_label_act(header, self.classes)  # output binary array
        label = torch.tensor(label, dtype=torch.float32)

        return (recording, label)

    def get_labels(self):
        headers = self.header_files.apply(load_challenge_data_header)
        multihot_labels = headers.apply(lambda h: get_label_act(h, self.classes))
        multihot_labels = np.array(multihot_labels.values.tolist())
        return multihot_labels

    def get_pos_weights(self):
        pos_weights = {}
        for i, c in enumerate(self.classes):
            n_pos = len(self.labels[self.labels[:, i] == 1])
            n_neg = len(self.labels[self.labels[:, i] == 0])

            try:
                pos_weights[c] = math.ceil(n_neg / n_pos)
            except ZeroDivisionError:
                pos_weights[c] = 1

        return pos_weights


class MultilabelBalancedRandomSampler(Sampler):
    """
    MultilabelBalancedRandomSampler: Given a multilabel dataset of length n_samples and
    number of classes n_classes, samples from the data with equal probability per class
    effectively oversampling minority classes and undersampling majority classes at the
    same time. Note that using this sampler does not guarantee that the distribution of
    classes in the output samples will be uniform, since the dataset is multilabel and
    sampling is based on a single class. This does however guarantee that all classes
    will have at least batch_size / n_classes samples as batch_size approaches infinity
    """

    def __init__(self, labels, indices=None, class_choice="cycle"):
        """
        Parameters:
        -----------
            labels: a multi-hot encoding numpy array of shape (n_samples, n_classes)
            indices: an arbitrary-length 1-dimensional numpy array representing a list
            of indices to sample only from.
            class_choice: a string indicating how class will be selected for every
            sample.
                "random": class is chosen uniformly at random.
                "cycle": the sampler cycles through the classes sequentially.
        """
        self.labels = labels
        self.indices = indices
        if self.indices is None:
            self.indices = range(len(labels))
        self.map = []
        for class_ in range(self.labels.shape[1]):
            pos_indices = np.where(self.labels[:, class_] == 1)[0]
            pos_indices = pos_indices[np.isin(pos_indices, self.indices)]
            self.map.append(pos_indices)

        assert class_choice in ["random", "cycle"]
        self.class_choice = class_choice
        self.current_class = 0

    def __iter__(self):
        self.count = 0
        return self

    def __next__(self):
        if self.count >= len(self.indices):
            raise StopIteration
        self.count += 1
        try:
            return self.sample()
        except ValueError: # No positive sample found for the current class
            return np.random.choice(self.indices)

    def sample(self):
        if self.class_choice == "random":
            class_ = random.randint(0, self.labels.shape[1] - 1)
        elif self.class_choice == "cycle":
            class_ = self.current_class
            self.current_class = (self.current_class + 1) % self.labels.shape[1]
        pos_indices = self.map[class_]
        return np.random.choice(pos_indices)

    def __len__(self):
        return len(self.indices)


def pad_or_truncate(array, output_len=SEQ_LENGTH):
    length = len(array)

    if length > output_len:  # Take middle subarray
        begin = int((length - output_len) / 2)
        end = int((length + output_len) / 2)
        return array[begin:end]

    if length < output_len:  # pre-pad
        return np.pad(array, pad_width=((output_len - length, 0), (0, 0)))

    else:
        return array

# Load challenge data.
def load_challenge_data_recording(header_file):
    mat_file = header_file.replace('.hea', '.mat')
    x = loadmat(mat_file)
    recording = np.asarray(x['val'], dtype=np.float64)
    return np.swapaxes(recording, 0,1) # -> shape (seq_length, n_leads=12)

def load_challenge_data_header(header_file):
    with open(header_file, 'r') as f:
        header = f.readlines()
    return header

# Get sparse matrix class labels
def get_label_act(header, classes):
    labels_act = np.zeros(len(classes))
    for l in header[::-1]:
        if l.startswith('#Dx:'):
            arrs = l.strip().split(' ')
            for arr in arrs[1].split(','):
                arr = arr.rstrip()
                if arr in EQUIVALENT_ABNORMALITIES.keys():
                    arr = EQUIVALENT_ABNORMALITIES[arr]
                if arr in classes:
                    class_index = classes.index(arr)
                    labels_act[class_index] = 1
    return labels_act
