#!/usr/bin/env python

import os, sys
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import torch.optim as optim
from torchsummary import summary
from datetime import datetime
import numpy as np
from sklearn.metrics import precision_recall_curve, auc
from load_datasets import PhysioNetDataset, SEQ_LENGTH, MultilabelBalancedRandomSampler
from models import Conv_Transformer, xavier_init_weights

device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
print(f"device: {str(device).upper()}")

MODEL_FILE = "model_checkpoint.pt"

def train_model(input_directory, model_directory):
    print('Loading data...')

    batch_size = 256
    data_train = PhysioNetDataset(data_dir=input_directory, purpose="train")
    classes = data_train.classes
    train_sampler = MultilabelBalancedRandomSampler(data_train.labels)
    train_loader = DataLoader(data_train, batch_size=batch_size, sampler=train_sampler, drop_last=False)

    data_val = PhysioNetDataset(data_dir=input_directory, purpose="test")

    # Define a model and initialize weights
    model = Conv_Transformer(classes=classes, n_layers=1, n_heads=2,
                             dim_feedforward=512, dropout=0.5)

    model.apply(xavier_init_weights)
    model.to(device)
    summary(model, input_size=(SEQ_LENGTH, 12), batch_size=batch_size, device=str(device))

    print(f'Training model ...')

    n_batches = len(train_loader)
    n_epochs = 50
    lr = 0.001

    criterion_train = nn.BCEWithLogitsLoss(pos_weight=None)
    optimizer = optim.Adam(model.parameters(), lr=lr, weight_decay=0.007)

    scheduler = optim.lr_scheduler.CosineAnnealingWarmRestarts(optimizer, T_0=n_batches)
    best_auc = 0.0
    start = datetime.now()

    for epoch in range(n_epochs):
        print(f"\nEpoch: {epoch}")

        # Training
        model.train()
        losses = torch.tensor([], dtype=torch.float32, device=device)
        for i, (X_train, y_train) in enumerate(train_loader):
            X_train, y_train = X_train.to(device), y_train.to(device)

            optimizer.zero_grad()
            y_hat = model(X_train)
            loss = criterion_train(y_hat, y_train)
            loss.backward()
            optimizer.step()
            scheduler.step()

            losses = torch.cat([losses, loss.reshape(1)]) # Append the loss of each batch
            if i % 100 == 0:
                print(f"Batch {i}: loss = {loss.item():.4f}")

            # Free up GPU memory
            del X_train, y_train, y_hat
            torch.cuda.empty_cache()

        mean_loss = torch.mean(losses)
        print(f"Mean train loss : {mean_loss.item():.4f}")

        # Eval on the training set
        mean_loss_train, y_trues_train, y_scores_train = eval(model=model, datatest=data_train,
                                                              pos_weight=None, batch_size=batch_size)
        auprc_train = get_auprc(y_trues_train, y_scores_train)
        print("AUPRCs on training set:", [float(f"{p:.4f}") for p in auprc_train])
        print(f"Mean AUPRCs on training set: {np.mean(auprc_train):.4f}")

        # Eval on the test set
        mean_test_loss, y_trues, y_scores = eval(model=model, datatest=data_val,
                                                 pos_weight=None, batch_size=batch_size)
        print(f"Mean val loss: {mean_test_loss:.4f}")

        auprc = get_auprc(y_trues, y_scores)
        mean_auc = np.mean(auprc)
        print(f"Mean AUPRCs on test set: {mean_auc:.4f}")

        # Save checkpoint
        if mean_auc > best_auc:
            best_auc = mean_auc
            print("AUPRCs:", [float(f"{p:.4f}") for p in auprc])
            print(f"Min AUPRCs: {np.min(auprc):.4f}")
            print(f"Max AUPRCs: {np.max(auprc):.4f}")
            print(f"STD of AUPRCs: {np.std(auprc):.4f}")

            print(f"Best mean AUPRCs = {np.mean(auprc):.4f}, saving checkpoint ...")

            filename = os.path.join(model_directory, MODEL_FILE)
            checkpoint = {
                'model': model,
                'state_dict': model.state_dict(),
            }

            torch.save(checkpoint, filename)
            print("Saved!")

        del losses, mean_test_loss, y_trues, y_scores, y_trues_train, y_scores_train
        torch.cuda.empty_cache()

    print(f"training time: {datetime.now() - start}")

def eval(model, datatest, pos_weight=None, batch_size=64):
    testloader = DataLoader(dataset=datatest, batch_size=batch_size, drop_last=False)

    criterion = torch.nn.BCEWithLogitsLoss(pos_weight=pos_weight)
    total_loss = 0.0

    y_trues = np.empty((0, len(datatest.classes)))
    y_scores = np.empty((0, len(datatest.classes)))

    model.eval()
    with torch.no_grad():
        for i, (X_test, y_test) in enumerate(testloader):
            X_test, y_test = X_test.to(device), y_test.to(device)
            y_hat = model(X_test)
            loss = criterion(y_hat, y_test)

            y_score = torch.sigmoid(y_hat).cpu().numpy()
            y_test = y_test.cpu()

            y_scores = np.concatenate((y_scores, y_score), axis=0)
            y_trues = np.concatenate((y_trues, y_test), axis=0)
            total_loss += loss.item()

            del X_test, y_test, y_hat, loss
            torch.cuda.empty_cache()

    mean_loss = total_loss / len(testloader)
    return mean_loss, y_trues, y_scores


def get_auprc(y_trues, y_scores):
    auprc = []
    for j in range(y_trues.shape[1]):
        p, r, thresholds = precision_recall_curve(y_trues[:, j], y_scores[:, j])
        auprc.append(auc(r, p))

    return np.array(auprc)


if __name__ == '__main__':
    input_directory = sys.argv[1]
    model_directory = sys.argv[2]

    if not os.path.isdir(model_directory):
        os.mkdir(model_directory)

    train_model(input_directory, model_directory)
