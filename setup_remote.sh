#!/bin/bash

### Setup training on a remote machine (a Cloud server or Google Colab)

# Clone this repo
git clone https://github.com/cuongvng/ECG-multi-diagnosis-with-Transformer ~/transformer-12-ecg && cd ~/transformer-12-ecg/

# Download data: you can copy and paste all those lines at once
mkdir data && cd data
wget -O PhysioNetChallenge2020_Training_PTB-XL.tar.gz \
https://cloudypipeline.com:9555/api/download/physionet2020training/PhysioNetChallenge2020_PTB-XL.tar.gz/
wget -O PhysioNetChallenge2020_Training_E.tar.gz \
https://cloudypipeline.com:9555/api/download/physionet2020training/PhysioNetChallenge2020_Training_E.tar.gz/
wget -O PhysioNetChallenge2020_Training_CPSC.tar.gz \
https://cloudypipeline.com:9555/api/download/physionet2020training/PhysioNetChallenge2020_Training_CPSC.tar.gz/
wget -O PhysioNetChallenge2020_Training_2.tar.gz \
https://cloudypipeline.com:9555/api/download/physionet2020training/PhysioNetChallenge2020_Training_2.tar.gz/
tar -xzf PhysioNetChallenge2020_Training_CPSC.tar.gz && tar -xzf PhysioNetChallenge2020_Training_2.tar.gz
tar -xzf PhysioNetChallenge2020_Training_PTB-XL.tar.gz && tar -xzf PhysioNetChallenge2020_Training_E.tar.gz
rm *tar.gz
cd ./WFDB && mv * ../ && cd ..
cd ./Training_2 && mv * ../ && cd ..
cd ./Training_WFDB && mv * ../ && cd ..
rm -rf Training_2/ Training_WFDB/ WFDB/

# Install Conda
cd /tmp
curl -O https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
bash Miniconda3-latest-Linux-x86_64.sh
# Then follow the instructions, type `ENTER` -> `yes` -> `ENTER` -> `yes`.

# Activate the environment and install required packages
source ~/.bashrc
conda create --name physionet -y && conda activate physionet
cd ~/transformer-12-ecg/
conda install -c conda-forge python=3.7.8 -y
pip install -r requirements.txt
