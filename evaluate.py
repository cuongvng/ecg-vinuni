from predict import load_checkpoint, device
from train import get_auprc, eval
from load_datasets import *
import torch
import numpy as np
import joblib


if __name__ == "__main__":
	datatest = PhysioNetDataset(data_dir="data", purpose="test")
	model = load_checkpoint()

	pos_weights = torch.tensor(list(datatest.get_pos_weights().values()), device=device)
	mean_test_loss, y_trues, y_scores = eval(model, datatest, pos_weights, batch_size=64)
	print(f"Average BCE loss on test set: {mean_test_loss:.4f}")

	pr_auc = get_auprc(y_trues, y_scores)

	print("AUPRCs:", [float(f"{p:.4f}") for p in pr_auc])
	print(f"Mean AUPRCs: {np.mean(pr_auc):.4f}")
	print(f"Min AUPRCs: {np.min(pr_auc):.4f}")
	print(f"Max AUPRCs: {np.max(pr_auc):.4f}")
	print(f"STD of AUPRCs: {np.std(pr_auc):.4f}")

	joblib.dump(y_trues, "./model/y_trues.joblib")
	joblib.dump(y_scores, "./model/y_scores.joblib")